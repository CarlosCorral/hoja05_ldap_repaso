# Hoja05 LDAP Respaso


#### Ejercicio1

 1. Crea un repositorio en tu máquina y crea en él un fichero README.md en el que realices la documentación de los siguientes ejercicios.


 2. Al finalizar cada ejercicio realiza un push para que se suba a un repositorio público en GitHub o GitLab.

 ![Texto alternativo](Imagenes/Captura1.PNG)

#### Ejercicio2

 3. Crear en LDAP una nueva organizationalUnit llamada alumnos. Crea también 6 usuarios pertenecientes a esa unidad. Por último, crea dos grupos (groupOfUniqueNames) llamados daw1 y daw2. De los 6 usuarios creados antes, habrá 3 en cada grupo.
 Para ello harás un fichero con extensión ldif que deberás añadir al LDAP. 

 
![Texto alternativo](Imagenes/Captura4.PNG)

![Texto alternativo](Imagenes/Captura2.PNG)

![Texto alternativo](Imagenes/Captura3.PNG)

4. Crea a continuación un proyecto dinámico en Eclipse (Dynamic Web Project) llamado repaso. En él haz un fichero index.jsp que muestre la hora actual. 

![Texto alternativo](Imagenes/Captura6.PNG)

![Texto alternativo](Imagenes/Captura7.PNG)

5. A continuación, configuraremos la aplicación repaso utilizando JNDIRealm para que sólo puedan acceder los alumnos del grupo daw2.

![Texto alternativo](Imagenes/Captura8.PNG)

![Texto alternativo](Imagenes/Captura9.PNG)


 6. Por último, despliega la aplicación utilizando Ant (que deberás configurar previamente en ella). RECUERDA SUBIR LA DOCUMENTACIÓN A GIT

![Texto alternativo](Imagenes/Captura10.PNG)

#### Ejercicio3

 7. Crea un host virtual llamado repaso.com y despliega la aplicación en él.

 ![Texto alternativo](Imagenes/Captura11.PNG)


 8. Haz que la aplicación sólo funcione por SSL RECUERDA SUBIR LA DOCUMENTACIÓN A GIT
